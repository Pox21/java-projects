import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

public class Main {
    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_GREEN = "\u001B[32m";
    final public static String DOG = "dog";
    final public static String OWNER = "owner";
    public static Map<String, List<String>> getPetsByOwner(List<Map<String, String>> pets) {
        Map<String, List<String>> petsByOwner = new HashMap<>();

        for (Map<String, String> item : pets) {
            String dog = item.get(DOG);
            String owner = item.get(OWNER);
            if (!petsByOwner.containsKey(owner)) {
                petsByOwner.put(item.get(OWNER), new ArrayList<>());
            }
            petsByOwner.get(owner).add(dog);
        }

        return petsByOwner;
    }
    public static Map<String, String> buildRecord(String dogName, String ownerName) {
        Map<String, String> record = new HashMap<>();
        record.put(DOG, dogName);
        record.put(OWNER, ownerName);
        return record;
    }
    public static Map<String, String> addDog() throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.print("Введите имя собаки: ");
        String dogName = br.readLine();
        System.out.print("Введите имя владельца: ");
        String ownerName = br.readLine();

        return buildRecord(dogName, ownerName);
    }

    public static void printList (Map<String, List<String>> petsByOwner) {
        System.out.println("{");
        for (String owner : petsByOwner.keySet()) {
            System.out.printf(" \"%s%s%s\": [%n", ANSI_GREEN, owner, ANSI_RESET);
            for (String item : petsByOwner.get(owner)) {
                System.out.printf("     \"%s\",%n", item);
            }
            System.out.print("   ]\n");
        }
        System.out.println("}");
    }
    public static void main(String[] args) throws IOException {
        List<Map<String, String>> info = new ArrayList<>();
        for (int i = 0; i < 3; i++) info.add(addDog());
        Map<String, List<String>> petsByOwner = getPetsByOwner(info);
        printList(petsByOwner);
    }
}