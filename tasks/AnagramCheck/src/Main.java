import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

public class Main {
    public static String isAnagram(String firstStr, String secondStr) {
        if (firstStr.length() != secondStr.length()) return "NO";
        List<String> strOne = new ArrayList<>(Arrays.asList(firstStr.toLowerCase().split("")));
        List<String> strSecond = new ArrayList<>(Arrays.asList(secondStr.toLowerCase().split("")));
        Collections.sort(strOne);
        Collections.sort(strSecond);
        for (int i = 0; i < strOne.size(); i++) {
            if (!Objects.equals(strOne.get(i), strSecond.get(i))) return "NO";
        }
        return "YES";
    }

    public static String readString() throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.print("Введите слово: ");
        return br.readLine();
    }

    public static void main(String[] args) throws IOException {
        boolean stop = false;
        do {
            String firstWord = readString();
            String secondWord = readString();
            System.out.println(isAnagram(firstWord, secondWord));
            System.out.println("Again ? : y/n");
            stop = readString().toLowerCase().charAt(0) == 'y';
        } while (stop);
    }
}