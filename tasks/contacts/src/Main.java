import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Main {

    final public static String NAME = "name";
    final public static String PHONE = "phone";
    final public static String EMAIL = "e_mail";
    public static void main(String[] args) throws IOException {

        List<Map<String, String>> records = new ArrayList<>();
        for (int i = 0; i < 2; i++) {
            Map<String, String> info = readRecord();
            records.add(info);
        }
        System.out.println("-------------------");

        for (Map<String, String> x : records) {
            printRecord(x);
        }

    }
    public static void printRecord(Map<String, String> record) {
        System.out.printf("ИМЯ:     - %s%n", record.get(NAME));
        System.out.printf("Телефон: - %s%n", record.get(PHONE));
        System.out.printf("email:   - %s%n", record.get(EMAIL));
        System.out.println("-------------------");
    }
    public static Map<String, String> readRecord() throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        System.out.print("Введите имя: ");
        String name = br.readLine();
        System.out.print("Введите номер телефон: ");
        String phone = br.readLine();
        System.out.print("Введите эмаил: ");
        String email = br.readLine();

        return buildRecord(name, phone, email);
    }
    public static Map<String, String> buildRecord(String name, String phone, String email) {
        Map<String, String> record = new HashMap<>();
        record.put(NAME, name);
        record.put(PHONE, phone);
        record.put(EMAIL, email);
        return record;
    }
}