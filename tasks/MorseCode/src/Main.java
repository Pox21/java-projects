import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

public class Main {
    final public static Map<Character, String> code = new HashMap<>(){{
        put('A', ".-");   put('J', ".---");
        put('B', "-..."); put('K', "-.-");
        put('C', "-.-."); put('L', ".-..");
        put('D', "-..");  put('M', "--");
        put('E', ".");    put('N', "-.");
        put('F', "..-."); put('O', "---");
        put('G', "--.");  put('P', ".--.");
        put('H', "...."); put('Q', "--.-");
        put('I', "..");   put('R', ".-.");

        put('S', "...");  put('1', ".----");
        put('T', "-");    put('2', "..---");
        put('U', "..-");  put('3', "...--");
        put('V', "...-"); put('4', "....-");
        put('W', ".--");  put('5', ".....");
        put('X', "-..-"); put('6', "-....");
        put('Y', "-.--"); put('7', "--...");
        put('Z', "--.."); put('8', "---..");
        put('0', "-----");put('9', "----.");

        put(' ', " ");
    }};
    private static void codeMapFill() {
        code.put('A', ".-");   code.put('J', ".---");
        code.put('B', "-..."); code.put('K', "-.-");
        code.put('C', "-.-."); code.put('L', ".-..");
        code.put('D', "-..");  code.put('M', "--");
        code.put('E', ".");    code.put('N', "-.");
        code.put('F', "..-."); code.put('O', "---");
        code.put('G', "--.");  code.put('P', ".--.");
        code.put('H', "...."); code.put('Q', "--.-");
        code.put('I', "..");   code.put('R', ".-.");

        code.put('S', "...");   code.put('1', ".----");
        code.put('T', "-"); code.put('2', "..---");
        code.put('U', "..-"); code.put('3', "...--");
        code.put('V', "...-");  code.put('4', "....-");
        code.put('W', ".--");    code.put('5', ".....");
        code.put('X', "-..-"); code.put('6', "-....");
        code.put('Y', "-.--");  code.put('7', "--...");
        code.put('Z', "--.."); code.put('8', "---..");
        code.put('0', "-----");   code.put('9', "----.");

        code.put(' ', " ");
    }

    public static String encodeString(String str) {
        StringBuilder encodeStr = new StringBuilder();
        String strToUpperCase = str.toUpperCase();
        for (int i = 0; i < strToUpperCase.length(); i++) {
            if (!code.containsKey(strToUpperCase.charAt(i))) continue;
            encodeStr.append(code.get(strToUpperCase.charAt(i)));
        }

        return encodeStr.toString();
    }
    public static String readString() throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.print("Введите сообщение: ");
        return br.readLine();
    }
    public static void main(String[] args) throws IOException {
//        codeMapFill();
        String str = readString();
        System.out.println(encodeString(str));
    }
}