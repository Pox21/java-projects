import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class Main {
    final public static char SEP = ':'; // разделитель
    final public static String PATH = "tasks/PhoneBook/res/"; // путь до файлов
    final public static String NAME = "name";
    final public static String PHONE = "phone";
    public static List<Map<String, String>> readFile(File phoneBook) {
        // метод для чтения файла и возврата списка контактов, возвращает список контактов
        // принимает файл
        List<Map<String, String>> book = new ArrayList<>();
        if (!phoneBook.exists()) {
            System.out.println("Phonebook is empty!");
            return book;
        } // если файла нет, возвращаем пустую книгу

        try (BufferedReader br = new BufferedReader(new FileReader(phoneBook))) {
            for (String line = br.readLine(); line != null; line = br.readLine()) {
                int lastSep = line.lastIndexOf(SEP);
                String name = line.substring(0, lastSep);
                String phone = line.substring(lastSep + 1);

                Map<String, String> entry = new HashMap<>();
                entry.put(NAME, name);
                entry.put(PHONE, phone);
                book.add(entry);
            }
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }

        return book;
    }

    public static boolean recordContact(File phoneBook, List<Map<String, String>>records) {
        // метод для записи в файл списка контактов возвращает булево
        // принимает файл и список контактов
        boolean isAdded = false;
        try (FileWriter fr = new FileWriter(phoneBook)){
            if (!phoneBook.exists()) { // если файла нет, создать новый файл
                phoneBook.createNewFile();
                System.out.println("Phonebook is empty!");
            }
            for (Map<String, String> entry : records) {
                String nameFromBook = entry.get("name");
                String phoneFromPhone = entry.get("phone");
                fr.write(nameFromBook + SEP + phoneFromPhone + "\n");
            }
            isAdded = true;
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }

        return true;
    }

    public static void addContact(List<Map<String, String>> records, String name, String phone) {
        // метод для добавления записи в список контактов, принимает список, имя и телефон для добавления
        // ничего не возвращает
        Map<String, String> book = new HashMap<>();
        book.put(NAME, name);
        book.put(PHONE, phone);
        records.add(book);
    }

    public static String readInfo(String title) {
        // метод для чтения данных с клавиатуры
        // возвращает строку
        String str = "";
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            System.out.print(title);
            str = br.readLine();
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
        return str;
    }

    public static int countContacts() {
        // метод для определения количества добавляемых записей
        // возвращает число
        int count = 1;
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            System.out.print("Enter count contacts: ");
            count = Integer.parseInt(br.readLine());
        } catch (IOException | NumberFormatException e) {
            System.out.println(e.getMessage());
        }
        return count;
    }
    public static void main(String[] args) {
        File phoneBook = new File(PATH + "phonebook.txt");

        List<Map<String, String>> records = readFile(phoneBook);

        int countContacts = countContacts();
        for(int i = 0; i < countContacts; i++) {
            String name = readInfo("Enter contact name: ");
            String phone = readInfo("Enter contact phone: ");

            addContact(records, name, phone);
            if (!recordContact(phoneBook, records)) {
                System.out.println("New contact not added.");
            }
        }
    }
}
