import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Objects;
public class Main {
    public static String[] generateAnagram(String name) {
        String[] anagramResult = new String[name.length()];
        ArrayList<String> anagramList = new ArrayList<>(
            Arrays.asList(name.toLowerCase().split(""))
        );
        for (int i = 0; i < name.length(); i++) {
            Collections.shuffle(anagramList);
            String str = "";
            for (String item : anagramList) str += item;
            for (String item : anagramResult) {
                if (Objects.equals(item, str) && Objects.equals(str, name.toLowerCase()));
                else anagramResult[i] = str;
            }
        }
        return anagramResult;
    }
    public static String[] getAnagrams(ArrayList<String> arr, String name) {
        HashMap<String, String[]> nameAndAnagrams = new HashMap<>();
        for (String nameOfList : arr) {
            nameAndAnagrams.put(nameOfList.toLowerCase(), generateAnagram(nameOfList.toLowerCase()));
        }
        return nameAndAnagrams.get(arr.get(arr.indexOf(name)).toLowerCase());
    }

    public static void main(String[] args) {
        String[] words = {"Егор", "Мария", "Андрей", "Артур"};
        ArrayList<String> names = new ArrayList<>( Arrays.asList(words) );
        String name = "Андрей";
        System.out.println("\n---------------");
        System.out.printf("Анаграмы от имени '%s' =", name);
        for (String str : getAnagrams(names, name)) System.out.printf(" %s", str);
        System.out.println("\n---------------");


            System.out.printf(
                "%sПри %sжелании %sлюбой %sцвет %sможно %s %sиспользовать %s",
                ANSI_YELLOW, ANSI_GREEN, ANSI_BLUE, ANSI_PURPLE, ANSI_RED, ANSI_BLACK_BACKGROUND, ANSI_CYAN, ANSI_RESET
            );

    }
        public static final String ANSI_RESET = "\u001B[0m";
        public static final String ANSI_BLACK = "\u001B[30m";
        public static final String ANSI_RED = "\u001B[31m";
        public static final String ANSI_GREEN = "\u001B[32m";
        public static final String ANSI_YELLOW = "\u001B[33m";
        public static final String ANSI_BLUE = "\u001B[34m";
        public static final String ANSI_PURPLE = "\u001B[35m";
        public static final String ANSI_CYAN = "\u001B[36m";
        public static final String ANSI_BLACK_BACKGROUND = "\u001B[40m";
        public static final String ANSI_WHITE = "\u001B[37m";
}
