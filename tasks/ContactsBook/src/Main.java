import java.util.*;

public class Main {
    public static void main(String[] args) {
        String[] students = { // массив студентов
            "Полина Авдеева",
            "Василиса Васильева",
            "Даниил Денисов",
            "София Егорова",
            "Степан Иванов",
            "Полина Иванова",
            "Максим Кириллов",
            "Юрий Ковалев",
            "Анастасия Козлова",
            "Мария Козлова",
            "Артём Королев",
            "Елизавета Кузнецова",
            "Роман Марков",
            "Татьяна Матвеева",
            "Валерия Покровская",
            "Даниил Румянцев",
            "Виктор Сергеев",
            "Иван Соколов",
            "Мария Толкачева",
            "Алексей Чернов"
        };
        int studentsCount = 20;
        Random random = new Random();
        String[] studentsPhoneNumber = new String[studentsCount];// массив номеров телефонов
        Arrays.fill(studentsPhoneNumber, "+49 1" + random.nextInt(999999999));

        TreeMap<String, String> contacts = new TreeMap<>(); // объединяем массив имен и номеров
        for (int i = 0; i < students.length; i++) {
            contacts.put(students[i], studentsPhoneNumber[i]);
        }
        HashMap<Integer, TreeMap<String, String>> contactsOfStudents = new HashMap<>();
        int counter = 1;
        for (String key : contacts.keySet()) {
            TreeMap<String, String> student = new TreeMap<>();
            student.put(key, contacts.get(key));
            contactsOfStudents.put(counter, student);
            counter++;
        }

        System.out.println(contactsOfStudents);
    }
}