import java.io.*;

public class Main {
    final public static String PATH = "tasks/MyFirsCRUD/res";
    // метод для считывания с файла
    public static int readFile(File filePath) throws IOException {
        int num = 0;
        BufferedReader br = new BufferedReader(new FileReader(filePath));
        try {
            num = Integer.parseInt(br.readLine());
        } catch (NumberFormatException e) {
            System.out.println(e.getMessage());
        } finally {
            br.close();
        }
        return num;
    }

    // Метод для записи в файл
    public static void recordToFile(int num) {
        try {
            File file = new File(PATH + "/out.txt");
            if (!file.exists())  file.createNewFile();

            FileWriter fr = new FileWriter(file, true);
            fr.write(Integer.toBinaryString(num) + "\n");
            fr.close();
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }
    public static void main(String[] args) throws IOException {
        File file =  new File(PATH + "/in.txt");

        int num = readFile(file);
        recordToFile(num);
    }
}