import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

public class Main {
    public static Map<Character, Integer> lettersCounter(String str) {
        Map<Character, Integer> letters = new HashMap<>();
        String strLower = str.toLowerCase();

        for (int i = 0; i < str.length(); i++) {
            char letter = strLower.charAt(i);
            if (!letters.containsKey(letter)) {
                letters.put(letter, 0);
            }
            letters.put(letter, letters.get(letter) + 1);
        }

        return letters;
    }
    public static String readString() throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        return br.readLine();
    }
    public static void printMap(Map<Character, Integer> letters) {
        for (char key : letters.keySet()) {
            System.out.printf("%s: %s%n", key, letters.get(key));
        }
    }
    public static void main(String[] args) throws IOException {
        Map<Character, Integer> letters = lettersCounter(readString());
        printMap(letters);
    }
}