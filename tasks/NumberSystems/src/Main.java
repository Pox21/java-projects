import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {

    public static String decToBin(int number) {
        int num = number;
        StringBuilder binaryNum = new StringBuilder();
        if (number == 0) return "0";
        if (number < 0) number = Math.abs(number);
        while (number != 0) {
            binaryNum.append(number % 2);
            number = number / 2;
        }
        System.out.printf("Число '%s' в двоичной системе -> ", num);
        return binaryNum.reverse().toString();
//        return Integer.toString(number, 2);
//        return Integer.toBinaryString(number);
    }

    public static int maxDecDigit(int number) {
        int num = number;
        int max = Integer.MIN_VALUE;
        while (number > 0) {
            int digit = number % 10;
            if (max < digit) max = digit;
            number /= 10;
        }
        System.out.printf("Самое большая цифра в числе '%s' -> ", num);
        return max;
    }

    public static String decToHex(int number) {
        int num = number;
        String result = "";
        int symbolCode = 'A';

        while (number > 0) {
            int digit = number % 16;
            String toPrint = digit > 9 ? "" + (char) (digit - 10 + symbolCode) : "" + digit;
            result = toPrint + result;
            number /= 16;
        }

        System.out.printf("Число '%s' в шестандцатеричной системе -> ", num);
        return result;
        // 675FF  423423
    }

    public static String hexToBin(String digits) {
        String digitsCode = "0123456789ABCDEF";
        digits = digits.toUpperCase();
        int num = 0;
        for (int i = 0; i < digits.length(); i++) {
            char c = digits.charAt(i);
            int d = digitsCode.indexOf(c);
            num = 16 * num + d;
        }
        System.out.printf("Шестандцетеричное число '%s' в двоичной системе -> ", digits);
        return Integer.toBinaryString(num);
    }

    public static int parseHexInt(String line) {
        String digitsCode = "0123456789ABCDEF";
        line = line.toUpperCase();
        int num = 0;
        for (int i = 0; i < line.length(); i++) {
            char c = line.charAt(i);
            int d = digitsCode.indexOf(c);
            num = 16 * num + d;
        }
        System.out.printf("Шестандцетеричное число '%s' в десятичной системе -> ", line);
        return num;
    }

    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
//        int num = Integer.parseInt(br.readLine());
        String num = br.readLine();

        System.out.println(decToBin(Integer.parseInt(num))); // 423423 -> 1100111010111111111
        System.out.println(maxDecDigit(Integer.parseInt(num))); // 423423 -> 4
        System.out.println(decToHex(Integer.parseInt(num))); // 423423 -> 675FF
        System.out.println(hexToBin(num)); // 1bc -> 110111100
        System.out.println(parseHexInt(num)); // 675FF -> 423423

    }


}