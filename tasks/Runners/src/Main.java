import java.util.HashMap;
import java.util.TreeMap;

public class Main {
    public static void main(String[] args) {
        HashMap<Double, String> runners = new HashMap<>();
        runners.put(10 + Math.round(Math.random() * 100.0) / 10.0, "France");
        runners.put(10 + Math.round(Math.random() * 100.0) / 10.0, "USA");
        runners.put(10 + Math.round(Math.random() * 100.0) / 10.0, "Italy");
        runners.put(10 + Math.round(Math.random() * 100.0) / 10.0, "England");
        runners.put(10 + Math.round(Math.random() * 100.0) / 10.0, "Kazakhstan");
        runners.put(10 + Math.round(Math.random() * 100.0) / 10.0, "Brazil");
        runners.put(10 + Math.round(Math.random() * 100.0) / 10.0, "Portugal");
        runners.put(10 + Math.round(Math.random() * 100.0) / 10.0, "Canada");
        runners.put(10 + Math.round(Math.random() * 100.0) / 10.0, "Germany");
        runners.put(10 + Math.round(Math.random() * 100.0) / 10.0, "Russia");

        int counter = 1;
        TreeMap<Double, String> sortedRunners = new TreeMap<>(runners);
        for (Double item : sortedRunners.keySet()) {
            System.out.printf(
                "%2s| Участник представляющие %s со счетом %s !%n",
                counter, runners.get(item), item
            );
            counter++;
        }
        double firstItemKey = sortedRunners.firstEntry().getKey();
        String firstItemValue = sortedRunners.firstEntry().getValue();
    }
}