import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

public class Main {
    public static Map<Character, Integer> code = new HashMap<>() {{
        put('.', 1); put(',', 11); put('?', 111); put('!', 1111); put(':', 11111);
        put('A', 2); put('B', 22); put('C', 222);
        put('D', 3); put('E', 33); put('F', 333);
        put('G', 4); put('H', 44); put('I', 444);
        put('J', 5); put('K', 55); put('L', 555);
        put('M', 6); put('N', 66); put('O', 666);
        put('P', 7); put('Q', 77); put('R', 777); put('S', 7777);
        put('T', 8); put('U', 88); put('V', 888);
        put('W', 9); put('X', 99); put('Y', 999); put('Z', 9999);
        put(' ', 0);
    }};

    public static String encodeString(String str) {
        StringBuilder encodeStr = new StringBuilder();
        String strToUpperCase = str.toUpperCase();
        for (int i = 0; i < strToUpperCase.length(); i++) {
            if (!code.containsKey(strToUpperCase.charAt(i))) continue;
            encodeStr.append(code.get(strToUpperCase.charAt(i)));
        }

        return encodeStr.toString();
    }

    public static String readString() throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.print("Введите сообщение: ");
        return br.readLine();
    }
    public static void main(String[] args) throws IOException {
        String str = readString();
        System.out.println(encodeString(str));
        System.out.println("4433555555666110966677755531111");
    }
}