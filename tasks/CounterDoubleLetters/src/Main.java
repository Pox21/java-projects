import java.util.ArrayList;
import java.util.HashSet;

public class Main {
    public static int counterDoubleLetters(String str) {
        String strToLowerCase = str.toLowerCase();
        int count = 0;
        HashSet<Character> arr = new HashSet<>();
        char letter;
        for(int i = 0; i < str.length(); i++) {
            letter = strToLowerCase.charAt(i);
            int letterCount = 0;
            for (int j = 0; j < str.length(); j++) {
                if (letter == strToLowerCase.charAt(j)) {
                    letterCount++;
                }
            }
            if (letterCount > 1) count++;
        }
        return count;
    }
    public static void main(String[] args) {
        String str = "Hello world!";
        System.out.printf("Количество дубликатов букв в строке - '%s' : %s.%n" , str, counterDoubleLetters(str));
    }
}