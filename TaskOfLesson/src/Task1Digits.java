import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Task1Digits {
    public static void printDigits(int number) {

        while (number > 0) {
            System.out.println(number % 10);
            number /= 10;
        }
        System.out.println();
    }

    public static int countDigits(int number) {
        if (number == 0) {
            System.out.println("Число равно 0");
            return 1;
        }
        if (number < 0) {
            System.out.println("Отрицательое число");
            number = Math.abs(number);
        }
        int counter = 0;
        while (number > 0) {
            System.out.print(number % 10);
            number /= 10;
            counter++;
        }
        System.out.println();
        return counter;
    }
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int x = Integer.parseInt(br.readLine());
        printDigits(x);
//        System.out.println(countDigits(x));

    }
}